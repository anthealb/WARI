//
//  Image+CoreDataProperties.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 28/02/22.
//
//

import Foundation
import CoreData


extension ImageData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ImageData> {
        return NSFetchRequest<ImageData>(entityName: "ImageData")
    }

    @NSManaged public var large: String?
    @NSManaged public var medium: String?
    @NSManaged public var original: String?
    @NSManaged public var small: String?
    @NSManaged public var thumb: String?
    @NSManaged public var id: UUID
    @NSManaged public var isAnImageOf: GameData?
    
    public var getLarge: String {
        large ?? defaultImg
    }
    
    public var getMedium: String {
        medium ?? defaultImg
    }
    
    public var getOriginal: String {
        original ?? defaultImg
    }
    
    public var getSmall: String {
        small ?? defaultImg
    }
    
    public var getThumb: String {
        thumb ?? defaultImg
    }
    
    public var getGame: GameData {
        isAnImageOf ?? GameData()
    }

}

extension ImageData : Identifiable {

}
