//
//  Category+CoreDataProperties.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 28/02/22.
//
//

import Foundation
import CoreData


extension Category {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category")
    }

    @NSManaged public var idCategory: String?
//    @NSManaged public var idGame: String?
    @NSManaged public var isACategoryOf: NSSet?

    public var getIdCategory: String {
        idCategory ?? "No IdCategory Found"
    }
    
//    public var getIdGame: String {
//        idGame ?? "No Game Found"
//    }
    
    public var getGames: [GameData] {
        let set = isACategoryOf as? Set<GameData> ?? []
        
        return Array(set)
    }
}

// MARK: Generated accessors for isACategoryOf
extension Category {

    @objc(addIsACategoryOfObject:)
    @NSManaged public func addToIsACategoryOf(_ value: GameData)

    @objc(removeIsACategoryOfObject:)
    @NSManaged public func removeFromIsACategoryOf(_ value: GameData)

    @objc(addIsACategoryOf:)
    @NSManaged public func addToIsACategoryOf(_ values: NSSet)

    @objc(removeIsACategoryOf:)
    @NSManaged public func removeFromIsACategoryOf(_ values: NSSet)

}

extension Category : Identifiable {

}
