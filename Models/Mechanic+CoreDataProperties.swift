//
//  Mechanic+CoreDataProperties.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 28/02/22.
//
//

import Foundation
import CoreData


extension Mechanic {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Mechanic> {
        return NSFetchRequest<Mechanic>(entityName: "Mechanic")
    }

//    @NSManaged public var idGame: String?
    @NSManaged public var idMech: String?
    @NSManaged public var isAMechanicOf: NSSet?
    
    public var getIdMech: String {
        idMech ?? "No IdMech Found"
    }
    
//    public var getIdGame: String {
//        idGame ?? "No Game Found"
//    }
    
    public var getGames: [GameData] {
        let set = isAMechanicOf as? Set<GameData> ?? []
        
        return Array(set)
    }

}

// MARK: Generated accessors for isAMechanicOf
extension Mechanic {

    @objc(addIsAMechanicOfObject:)
    @NSManaged public func addToIsAMechanicOf(_ value: GameData)

    @objc(removeIsAMechanicOfObject:)
    @NSManaged public func removeFromIsAMechanicOf(_ value: GameData)

    @objc(addIsAMechanicOf:)
    @NSManaged public func addToIsAMechanicOf(_ values: NSSet)

    @objc(removeIsAMechanicOf:)
    @NSManaged public func removeFromIsAMechanicOf(_ values: NSSet)

}

extension Mechanic : Identifiable {

}
