//
//  GameData+CoreDataProperties.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 28/02/22.
//
//

import Foundation
import CoreData
import SwiftUI


extension GameData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GameData> {
        return NSFetchRequest<GameData>(entityName: "GameData")
    }
    
    @NSManaged public var collection: Int16
    @NSManaged public var averageUserRating: Double
    @NSManaged public var gameDescription: String?
    @NSManaged public var idGame: String?
    @NSManaged public var maxPlayers: Int32
    @NSManaged public var minPlayers: Int32
    @NSManaged public var maxPlaytime: Int32
    @NSManaged public var minPlaytime: Int32
    @NSManaged public var minAge: Int32
    @NSManaged public var name: String?
    @NSManaged public var isContained: Collection?
    @NSManaged public var hasCategory: NSSet?
    @NSManaged public var hasImage: ImageData?
    @NSManaged public var hasMechanic: NSSet?
    
    public var getAverageUserRating: String {
        averageUserRating < 0.0 ? "ND" : String(format: "%.2f", averageUserRating)
    }
    
    public var getDescription: String {
        gameDescription ?? "No Description Found"
    }
    
    public var getIdGame: String {
        idGame ?? "No Game Found"
    }
    
    public var getMaxPlayers: Int? {
        maxPlayers < 1 ? nil : Int(maxPlayers)
    }
    
    public var getMinPlayers: Int? {
        minPlayers < 1 ? nil : Int(minPlayers)
    }
    
    public var getMaxPlaytime: Int? {
        maxPlaytime < 0 ? nil : Int(maxPlaytime)
    }
    
    public var getMinPlaytime: Int? {
        minPlaytime < 0 ? nil : Int(minPlaytime)
    }
    
    public var getMinAge: Int? {
        minPlaytime < 0 ? nil : Int(minAge)
    }
    
    public var getName: String {
        name ?? "No Name Found"
    }
    
    public var getCollection: LocalizedStringKey? {
        if collection == 0 {
            return "Owned"
        }
        if collection == 1 {
            return "Wishlist"
        }
        return nil
    }
    
//    public var getCollection: Collection {
//        isContained ?? Collection()
//    }
    
    public var getImage: ImageData {
        hasImage ?? ImageData()
    }
    
    public var getCategories: [Category] {
        let set = hasCategory as? Set<Category> ?? []
        
        return Array(set)
    }
    
    public var getMechanics: [Mechanic] {
        let set = hasMechanic as? Set<Mechanic> ?? []
        
        return Array(set)
    }

}

// MARK: Generated accessors for hasCategory
extension GameData {

    @objc(addHasCategoryObject:)
    @NSManaged public func addToHasCategory(_ value: Category)

    @objc(removeHasCategoryObject:)
    @NSManaged public func removeFromHasCategory(_ value: Category)

    @objc(addHasCategory:)
    @NSManaged public func addToHasCategory(_ values: NSSet)

    @objc(removeHasCategory:)
    @NSManaged public func removeFromHasCategory(_ values: NSSet)

}

// MARK: Generated accessors for hasMechanic
extension GameData {

    @objc(addHasMechanicObject:)
    @NSManaged public func addToHasMechanic(_ value: Mechanic)

    @objc(removeHasMechanicObject:)
    @NSManaged public func removeFromHasMechanic(_ value: Mechanic)

    @objc(addHasMechanic:)
    @NSManaged public func addToHasMechanic(_ values: NSSet)

    @objc(removeHasMechanic:)
    @NSManaged public func removeFromHasMechanic(_ values: NSSet)

}

extension GameData : Identifiable {

}
