//
//  Collection+CoreDataProperties.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 28/02/22.
//
//

import Foundation
import CoreData


extension Collection {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Collection> {
        return NSFetchRequest<Collection>(entityName: "Collection")
    }

    @NSManaged public var code: Int32
    @NSManaged public var name: String?
    @NSManaged public var contains: NSSet?
    
    public var getCode: String {
        code<0 ? "No Code" : "\(code)"
    }
    
    public var getName: String {
        name ?? "No Name Found"
    }

    public var getGames: [GameData] {
        let set = contains as? Set<GameData> ?? []
        
        return Array(set)
    }
}

// MARK: Generated accessors for contains
extension Collection {

    @objc(addContainsObject:)
    @NSManaged public func addToContains(_ value: GameData)

    @objc(removeContainsObject:)
    @NSManaged public func removeFromContains(_ value: GameData)

    @objc(addContains:)
    @NSManaged public func addToContains(_ values: NSSet)

    @objc(removeContains:)
    @NSManaged public func removeFromContains(_ values: NSSet)

}

extension Collection : Identifiable {

}
