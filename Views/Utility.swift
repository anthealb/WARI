//
//  Utility.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 15/02/22.
//

import Foundation

func minutesToHoursMinutes(_ minutes: Int) -> [Int] {
    return [minutes/60, minutes%60]
}
