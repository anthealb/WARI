//
//  DetailView.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 14/02/22.
//

// CREATE FUNCTION FOR + BUTTON
import SwiftUI

struct DetailView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(entity: GameData.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \GameData.collection, ascending: true)]) var gameData: FetchedResults<GameData>
    @FetchRequest(entity: ImageData.entity(), sortDescriptors: []) var imageData: FetchedResults<ImageData>
    
    var thisGame: GameDataAPI?
    @State var isExpanded: Bool = false
    @State var tagDetails: Bool = false
    @State var showActionSheet: Bool = false
    
    var body: some View {
        ZStack(alignment: .bottom) {
            Rectangle()
                .overlay(alignment: .top){
                    Rectangle()
                        .ignoresSafeArea()
                        .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height*0.5)
                        .foregroundColor(Color.white)
                }
                .ignoresSafeArea()
                .foregroundColor(Color("DarkerBackground"))
            ScrollView {
                gameImage
                    .padding(.bottom)
                VStack{ gameTitleTags
                        .padding(.top)
                    gameQuickInfo
                        .padding()
                    descriptionElement
                    VStack{}.frame(width: nil, height: 50)
                }
                .frame(width: UIScreen.main.bounds.width)
                .background(Color("DarkerBackground"))
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .toolbar{
            ToolbarItem(placement: .automatic) { plusButton }
        }
    }
    
    func checkIfInDatabase() -> Bool {
        if thisGame?.id != nil {
            if gameData.contains(where: { $0.getIdGame == thisGame!.id }) {
                return true
            }
        }
        return false
    }
    
    @ViewBuilder
    var gameImage: some View{
        VStack{
            AsyncImage(url: thisGame?.getImageURL() ?? URL(string: defaultImg)!, content: { image in
                image
                    .resizable()
                    .scaledToFit()
                    .frame(width: UIScreen.main.bounds.width*0.85, height: UIScreen.main.bounds.height*0.20)
                    .padding()
            }, placeholder: {
                ProgressView()
            })
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: UIScreen.main.bounds.height*0.23, maxHeight: UIScreen.main.bounds.height*0.23)
        .background(Color.white)
        .ignoresSafeArea()
    }
    
    @ViewBuilder
    var gameTitleTags: some View {
        VStack (alignment: .leading){
            if let gameName = thisGame?.name {
                Text(LocalizedStringKey(gameName))
                    .font(Font.custom("Commissioner-Thin_Regular", size: 24, relativeTo: .title3))
                    .fontWeight(.bold)
                    .foregroundColor(Color("AccentColor"))
            } else {
                Text("No Name Available")
                    .font(.title.bold())
                    .foregroundColor(Color("AccentColor"))
            }
            if let tags = getTags(thisGame) {
                Text(LocalizedStringKey(tags))
                    .font(Font.custom("Commissioner-Thin_Regular", size: 16, relativeTo: .callout))
                    .fontWeight(.light)
                    .foregroundColor(Color("Secondary").opacity(0.75))
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
        }
        .multilineTextAlignment(.leading)
        .padding()
        .frame(width: UIScreen.main.bounds.width*0.9, alignment: .leading)
        .background(Color.white)
        .cornerRadius(10)
    }
    
    @ViewBuilder
    var gameQuickInfo: some View {
        HStack(alignment: .top){
            VStack{
                Image(systemName: "timer")
                    .font(.title2.bold())
                    .foregroundColor(Color("Secondary"))
                    .padding(3)
                    .multilineTextAlignment(.center)
                Text(playtimeString(minPlaytime: thisGame?.min_playtime, maxPlaytime: thisGame?.max_playtime))
                    .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title2))
                    .fontWeight(.bold)
                    .foregroundColor(Color("AccentColor"))
                    .multilineTextAlignment(.center)
            }
            .frame(width: UIScreen.main.bounds.width*0.28)
            Divider()
            VStack{
                Image(systemName: "person.2")
                    .font(.title2.bold())
                    .foregroundColor(Color("Secondary"))
                    .padding(3)
                Text(playersString(minPlayers: thisGame?.min_players, maxPlayers: thisGame?.max_players))
                    .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title2))
                    .fontWeight(.bold)
                    .foregroundColor(Color("AccentColor"))
            }
            .frame(width: UIScreen.main.bounds.width*0.28)
            Divider()
            VStack{
                Image(systemName: "person.text.rectangle")
                    .font(.title2.bold())
                    .foregroundColor(Color("Secondary"))
                    .padding(3)
                Text(minAgeString(minAge:thisGame?.min_age))
                    .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title2))
                    .fontWeight(.bold)
                    .foregroundColor(Color("AccentColor"))
            }
            .frame(width: UIScreen.main.bounds.width*0.28)
        }
    }
    
    @ViewBuilder
    var descriptionElement: some View {
        VStack() {
            HStack {
                Text("Description")
                    .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title2))
                    .fontWeight(.bold)
                    .foregroundColor(Color("Secondary"))
                    .padding(.bottom, 1)
                Spacer()
//                Button(action: {
//                    isExpanded.toggle()
//                }) {
//                    if !isExpanded {
//                        Text("Show more")
//                            .font(Font.custom("Commissioner-Thin_Regular", size: 16, relativeTo: .callout))
//                            .padding(.leading)
//                    }
//                }
            }
            .frame(width: UIScreen.main.bounds.width*0.85, alignment: .leading)
            AttributedText(thisGame?.getDescription().stringByDecodingHTMLEntities ?? "Description Not Available")
//                .lineLimit(isExpanded ? nil : 10)
                .font(Font.custom("Commissioner-Thin_Regular", size: 18))
                .foregroundColor(Color("Secondary"))
                .frame(width: UIScreen.main.bounds.width*0.85, alignment: .leading)
//                .overlay(
//                    GeometryReader { proxy in
//                        if isExpanded {
//                            Button(action: {
//                                isExpanded.toggle()
//                            }) {
//                                Text("Show less")
//                                    .font(Font.custom("Commissioner-Thin_Regular", size: 16, relativeTo: .callout))
//                                    .padding(.top)
//                                    .background(Color("DarkerBackground"))
//                            }
//                            .frame(width: proxy.size.width, height: proxy.size.height+10, alignment: .bottomTrailing)
//                        }
//                    }
//                )
            Spacer()
        }
    }
    
    @ViewBuilder
    var checkButton: some View {
        Button(action: {
            showActionSheet.toggle()
        }, label: {
            Image(systemName: "checkmark")
        })
            .alert("Are you sure you want to remove this game from the collection?", isPresented: $showActionSheet) {
                Button("Cancel") {
                    showActionSheet.toggle()
                }
                Button(role: .destructive){
                    showActionSheet.toggle()
                } label: {
                    Text("Remove")
                }
            }
    }
    
    @ViewBuilder
    var plusButton: some View {
        Button(action: {
            showActionSheet.toggle()
        }, label: {
            Text("Add to")
                .font(Font.custom("Commissioner-Thin_Regular", size: 16))
        })
            .confirmationDialog(
                "Choose the collection",
                isPresented: $showActionSheet) {
                    Button {
                        var gameImage = ImageData(context: managedObjectContext)
                        gameImage = createImageCopy(gameImage)
                        var game = GameData(context: managedObjectContext)
                        game = createGameCopy(game)
                        
                        gameImage.isAnImageOf = game
                        game.collection = 0
                        
                        do {
                            try managedObjectContext.save()
                        } catch {
                            let nsError = error as NSError
                            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                        }
                    } label: {
                        Text("Add to Owned").font(Font.custom("Commissioner-Thin_Regular", size: 16))
                    }
                    Button {
                        var gameImage = ImageData(context: managedObjectContext)
                        gameImage = createImageCopy(gameImage)
                        var game = GameData(context: managedObjectContext)
                        game = createGameCopy(game)
                        
                        gameImage.isAnImageOf = game
                        game.collection = 1
                        
                        do {
                            try managedObjectContext.save()
                        } catch {
                            let nsError = error as NSError
                            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                        }
                    } label: {
                        Text("Add to Wishlist").font(Font.custom("Commissioner-Thin_Regular", size: 16))
                    }
                    Button("Cancel", role: .cancel) {
                        showActionSheet = false
                    }
                    //                }
                    //                .actionSheet(isPresented: $showActionSheet) {
                    //                    ActionSheet(title: Text("Add this game to a collection"),
                    //                                message: Text("Choose the collection"),
                    //                                buttons: [
                    //                                    .cancel(),
                    //                                    .default(
                    //                                        Text("Add to Owned"),
                    //                                        action: doSomething
                    //                                    ),
                    //                                    .default(
                    //                                        Text("Add to Wishlist"),
                    //                                        action: doSomething
                    //                                    )
                    //                                ]
                    //                    )
                    //                }
                }
    }
    
    func createImageCopy(_ gameImage: ImageData) -> ImageData {
        gameImage.id = UUID()
        gameImage.large = thisGame?.images.large
        gameImage.medium = thisGame?.images.medium
        gameImage.original = thisGame?.images.original
        gameImage.small = thisGame?.images.small
        gameImage.thumb = thisGame?.images.thumb
        
        return gameImage
    }
    
    func createGameCopy(_ game: GameData) -> GameData {
        game.idGame = thisGame?.id
        game.averageUserRating = thisGame?.average_user_rating ?? Double.leastNonzeroMagnitude
        game.name = thisGame?.name
        game.maxPlaytime = Int32(thisGame?.max_playtime ?? Int(Int32.min))
        game.minPlaytime = Int32(thisGame?.min_playtime ?? Int(Int32.min))
        game.maxPlayers = Int32(thisGame?.min_playtime ?? Int(Int32.min))
        game.minPlayers = Int32(thisGame?.max_players ?? Int(Int32.min))
        game.minAge = Int32(thisGame?.min_age ?? Int(Int32.min))
        game.gameDescription = thisGame?.description
        
        return game
    }
}

//struct DetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailView()
//    }
//}
