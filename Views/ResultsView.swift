//
//  ResultsView.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 23/02/22.
//

import SwiftUI

struct ResultsView: View {
    @State private var showingFilters = false
    @Binding var gamesArr: [GameDataAPI]?
    @Binding var queryForGame: QueryGame
    @Binding var isSearching: Bool
    var title: LocalizedStringKey
    
    var body: some View {
        ScrollView(showsIndicators: false){
            VStack{}.frame(width: nil, height: 20)
            VStack {
                if gamesArr != nil {
                    ForEach(gamesArr!, id: \.self) { game in
                        NavigationLink(destination: {
                            DetailView(thisGame: game)
                        }, label: {
                            ListElementResults(game: game, isLast: game == gamesArr?.last)
                        })
                    }
                } else {
                    ProgressView()
                        .padding()
                }
            }
            .padding(.horizontal)
            .frame(width: UIScreen.main.bounds.width*0.9)
            .background(Color.white)
            .cornerRadius(10)
            .clipped()
            VStack{}.frame(width: nil, height: 30)
        }
        .onChange(of: queryForGame) { terms in
            Task {
                do {
                    let query = createQuery(terms)
                    let search = try await getGameByQuery(query)
                    guard let search = search else { return }
                    
                    withAnimation { gamesArr = search }
                } catch {
                    print(error)
                }
            }
        }
    }
    
    @ViewBuilder
    var filterButton: some View {
        HStack {
            Button(action: {
                showingFilters.toggle()
            }, label: {
                Image(systemName: "line.3.horizontal.decrease.circle")
                    .font(.title2.bold())
            })
                .sheet(isPresented: $showingFilters, content: {
                    FiltersView(currentQuery: $queryForGame)
                })
        }
    }
    
    @ViewBuilder
    var backButton: some View {
        HStack {
            Button(action: {
                queryForGame = QueryGame()
                gamesArr = nil
            }, label: {
                HStack {
                    Image(systemName: "chevron.left")
                        .font(.body.bold())
                    Text("Back")
                }
            })
        }
    }
}

struct CategoryView: View {
    @Binding var gamesArr: [GameDataAPI]?
    @Binding var searchTerms: QueryGame
    var title: LocalizedStringKey
    
    var body: some View {
        ZStack {
            Rectangle()
                .foregroundColor(Color("DarkerBackground"))
                .ignoresSafeArea()
            ScrollView(showsIndicators: false){
                VStack{}.frame(width: nil, height: 20)
                VStack {
                    if gamesArr != nil {
                        ForEach(gamesArr!, id: \.self) { game in
                            NavigationLink(destination: {
                                DetailView(thisGame: game)
                            }, label: {
                                ListElementResults(game: game, isLast: game == gamesArr?.last)
                            })
                        }
                    } else {
                        ProgressView()
                            .padding()
                    }
                }
                .padding(.horizontal)
                .frame(width: UIScreen.main.bounds.width*0.9)
                .background(Color.white)
                .cornerRadius(10)
                .clipped()
                VStack{}.frame(width: nil, height: 30)
            }
        }
        .navigationBarTitle(title)
    }
}

//struct ResultsView_Previews: PreviewProvider {
//    static var previews: some View {
//        ResultsView(title: "Preview")
//    }
//}
