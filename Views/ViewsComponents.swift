//
//  ViewsComponents.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 16/02/22.
//

import SwiftUI

struct GameBox: View {
    var thisGame: GameDataAPI
    
    var body: some View {
        ZStack(alignment: .bottom){
            VStack {
                AsyncImage(url: thisGame.getImageURL(), content: { image in
                    image
                        .resizable()
                        .scaledToFill()
                        .scaledToFit()
                        .padding()
                }, placeholder: {
                    ProgressView()
                })
                    .frame(width: UIScreen.main.bounds.width*0.39, height: UIScreen.main.bounds.width*0.39)
                Spacer()
            }
            .background(Color.white)
            .frame(width: UIScreen.main.bounds.width*0.39, height: UIScreen.main.bounds.width*0.49)
            ZStack(alignment: .top){
            if let name = longName(thisGame.name) {
                VStack(alignment: .leading){
                    Text(name.title)
                        .font(Font.custom("Commissioner-Thin_Regular", size: 16))
                        .fontWeight(.semibold)
                    Text(name.subtit)
                        .font(Font.custom("Commissioner-Thin_Regular", size: 13))
                        .fontWeight(.light)
                }
                .multilineTextAlignment(.leading)
                .foregroundColor(Color("Secondary"))
                .padding(.horizontal, 10)
            } else {
                Text(thisGame.name)
                    .font(Font.custom("Commissioner-Thin_Regular", size: 16))
                    .fontWeight(.semibold)
                    .multilineTextAlignment(.leading)
                    .foregroundColor(Color("Secondary"))
                    .padding(.horizontal, 10)
            }
            }
            .padding(.vertical,5)
            .frame(width: UIScreen.main.bounds.width*0.39, height: UIScreen.main.bounds.width*0.15, alignment: .topLeading)
            .background(Color.white)
        }
        .cornerRadius(10)
        .clipped()
        .padding(.bottom)
        .padding(.leading, 4)
    }
}

struct HScrollBoxesElement: View {
    var title: LocalizedStringKey
    @Binding var games: [GameDataAPI]?
    @State var searchTerms = QueryGame()
    
    var body: some View {
        VStack{
            HStack {
                Text(title)
                    .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title))
                    .fontWeight(.bold)
                    .foregroundColor(Color("Secondary"))
                Spacer()
                NavigationLink(destination: {
                    CategoryView(gamesArr: $games, searchTerms: $searchTerms, title: title)
                        .searchable(text: $searchTerms.name, placement: .navigationBarDrawer(displayMode: .always))
                }, label: {
                    Text("SEE ALL")
                        .font(Font.custom("Commissioner-Thin_Regular", size: 13, relativeTo: .callout).smallCaps())
                        .fontWeight(.semibold)
                        .padding(.top, 5)
                })
            }
            .frame(width: UIScreen.main.bounds.width*0.9, alignment: .leading)
            ScrollView(.horizontal, showsIndicators: false){
                HStack {
                    if games != nil {
                        ForEach(games!.prefix(5), id: \.self) { game in
                            NavigationLink(destination: {
                                DetailView(thisGame: game)
                            }, label: {
                                GameBox(thisGame: game)
                                    .padding(.leading,10)
                                    .padding(.top,7)
                                    .shadow(color: Color("Secondary").opacity(0.25), radius: 7, x: 4, y: 4)
                            })
                        }
                    } else {
                        ForEach((1...5), id: \.self) { _ in
                            NavigationLink(destination: {
                                DetailView(thisGame: GameDataAPI())
                            }, label: {
                                GameBox(thisGame: GameDataAPI())
                                    .padding(.leading,10)
                                    .padding(.top,7)
                                    .shadow(color: Color("Secondary").opacity(0.25), radius: 5, x: 4, y: 4)
                            })
                        }
                    }
                }
                .padding(.leading,10)
            }
        }
    }
}

struct ListElementDiscovery: View {
    var image: String
    var category: LocalizedStringKey
    
    var body: some View {
        HStack {
            HStack {
                Image(image)
                    .font(.title3)
                    .frame(width: 25)
                    .padding(.trailing)
                    .foregroundColor(Color("Secondary"))
                Text(category)
                    .font(Font.custom("Commissioner-Thin_Regular", size: 18))
                    .fontWeight(.medium)
                    .foregroundColor(Color("Secondary"))
                Spacer()
                Image(systemName: "chevron.right")
                    .foregroundColor(Color("AccentColor"))
            }
        }
        .padding()
        .frame(width: UIScreen.main.bounds.width*0.9)
        .background(Color.white)
        .cornerRadius(10)
        .clipped()
        .shadow(color: Color("Secondary").opacity(0.20), radius: 7, x: 4, y: 4)
    }
}

struct ListElement: View {
    var image: String
    var category: LocalizedStringKey
    var isLast: Bool = false
    
    var body: some View {
        HStack {
            if isLast {
                HStack {
                    Image(image)
                        .frame(width: 25)
                        .padding(.trailing)
                    Text(category)
                        .font(Font.custom("Commissioner-Thin_Regular", size: 18))
                        .fontWeight(.medium)
                        .foregroundColor(Color("Secondary"))
                    Spacer()
                    Image(systemName: "chevron.right")
                        .foregroundColor(Color("AccentColor"))
                }
            } else {
                Image(image)
                    .frame(width: 25)
                    .padding([.trailing, .bottom])
                VStack {
                    HStack {
                        Text(category)
                            .font(Font.custom("Commissioner-Thin_Regular", size: 18))
                            .fontWeight(.medium)
                            .foregroundColor(Color("Secondary"))
                        Spacer()
                        Image(systemName: "chevron.right")
                            .foregroundColor(Color("AccentColor"))
                    }
                    Divider()
                }
            }
        }
    }
}

struct ListElementResults: View {
    var game: GameDataAPI
    var isLast: Bool = false
    
    var body: some View {
        VStack (alignment: .leading){
            HStack {
                AsyncImage(url: game.getImageURL(),
                           content: { image in
                    image
                        .resizable()
                        .scaledToFill()
                        .scaledToFit()
                        .padding()
                }, placeholder: {
                    ProgressView()
                })
                    .frame(width: UIScreen.main.bounds.width*0.25, height: UIScreen.main.bounds.height*0.1)
                VStack(alignment: .leading){
                    Text(game.name)
                        .font(Font.custom("Commissioner-Thin_Regular", size: 18, relativeTo: .title3))
                        .fontWeight(.semibold)
                        .foregroundColor(Color("Secondary"))
                    Spacer()
                    Text("time: ").font(Font.custom("Commissioner-Thin_Regular", size: 16)).fontWeight(.light).foregroundColor(Color("Secondary")) + Text(playtimeStringShort(minPlaytime: game.min_playtime, maxPlaytime: game.max_playtime)).font(Font.custom("Commissioner-Thin_Regular", size: 16)).fontWeight(.medium).foregroundColor(Color("AccentColor")) + Text("   ") + Text("players: ").font(Font.custom("Commissioner-Thin_Regular", size: 16)).fontWeight(.light).foregroundColor(Color("Secondary")) + Text(playersStringShort(minPlayers: game.min_players, maxPlayers: game.max_players)).font(Font.custom("Commissioner-Thin_Regular", size: 16)).fontWeight(.medium).foregroundColor(Color("AccentColor"))
                    
                }
                .font(.subheadline)
                .frame(height: UIScreen.main.bounds.height*0.06)
                .multilineTextAlignment(.leading)
                Spacer()
                Image(systemName: "chevron.right")
                    .foregroundColor(Color("AccentColor"))
                    .padding(.trailing, 5)
            }
            if !isLast { Divider() }
        }
    }
}

//struct ListElementResultsBIS: View {
//    var image: String
//    var name: LocalizedStringKey
//    var category: LocalizedStringKey
//    var isLast: Bool = false
//
//    var body: some View {
//        VStack (alignment: .leading){
//            HStack {
//                AsyncImage(url: URL(string: image),
//                           content: { image in
//                    image
//                        .resizable()
//                        .scaledToFill()
//                        .scaledToFit()
//                        .padding()
//                }, placeholder: {
//                    ProgressView()
//                })
//                .frame(width: UIScreen.main.bounds.width*0.25, height: UIScreen.main.bounds.height*0.1)
//                VStack(alignment: .leading){
//                    Text(name)
//                        .fontWeight(.semibold)
//                        .lineLimit(2)
//                        .foregroundColor(Color("Secondary"))
//                    Text(category)
//                        .lineLimit(1)
//                        .foregroundColor(Color("SecondaryBis"))
//                }
//                .multilineTextAlignment(.leading)
//                    Spacer()
//                    Image(systemName: "chevron.right")
//                        .foregroundColor(Color("AccentColor"))
//            }
//            if !isLast { Divider() }
//        }
//    }
//}

struct CheckBoxView: View {
    @Binding var checked: Bool
    
    var body: some View {
        Image(systemName: checked ? "checkmark.square.fill" : "square")
            .foregroundColor(checked ? Color(uiColor: UIColor.accent()) : Color(uiColor: UIColor.secondary()))
//            .onTapGesture {
//                self.checked.toggle()
//            }
    }
}

struct SearchBar: View {
    @Binding var searchText: String
    @Binding var searching: Bool
    
    var body: some View{
        HStack {
            ZStack {
                Rectangle()
                    .foregroundColor(.white)
                    .frame(height: UIScreen.main.bounds.height*0.045)
                    .cornerRadius(13)
                HStack {
                    Image(systemName: "magnifyingglass")
                        .foregroundColor(Color("Secondary"))
                    TextField("Search", text: $searchText){ startedEditing in
                        if startedEditing {
                            withAnimation {
                                searching = true
                            }
                        }
                    } onCommit: {
                        withAnimation {
                            searching = false
                        }
                    }
                    .font(Font.custom("Commissioner-Thin_Regular", size: 16))
                    .foregroundColor(Color("Secondary"))
                }
                .padding(.leading, 13)
            }
            if searching {
                Button(action: {
                    withAnimation{
                        searchText = ""
                        searching = false
                        UIApplication.shared.dismissKeyboard()
                    }
                }, label: {
                    Text("Cancel").font(Font.custom("Commissioner-Thin_Regular", size: 16))
                })
            }
        }
        .padding(.vertical)
    }
}

//struct ViewsComponents_Previews: PreviewProvider {
//    static var previews: some View {
//        SearchBar()
//    }
//}
