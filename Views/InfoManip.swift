//
//  TitleManip.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 15/02/22.
//

import Foundation
import UIKit
import SwiftUI

extension UIColor {
    class func accent() -> UIColor {
        return UIColor.init(named: "AccentColor")!
    }
    class func secondary() -> UIColor {
        return UIColor.init(named: "Secondary")!
    }
    class func lightBackground() -> UIColor {
        return UIColor.init(named: "Background")!
    }
    class func background() -> UIColor {
        return UIColor.init(named: "DarkerBackground")!
    }
    class func secondaryBis() -> UIColor {
        return UIColor.init(named: "SecondaryBis")!
    }
}

struct OurCateg: Hashable {
    var name: String
    var image: String
}

enum TimeIntervals: String, Equatable, CaseIterable {
    case notSelected = "Not Selected"
    case upTo10 = "up to 10'"
    case upTo30 = "up to 30'"
    case upTo60 = "up to 60'"
    case upTo90 = "up to 90'"
    case moreThan90 = "90'+"
    
//    var localizedName: LocalizedStringKey { LocalizedStringKey(rawValue) }
    
}

let genresStrings: [OurCateg] = [OurCateg(name: "Abstract", image: "abstract"),
                                 OurCateg(name: "Adventure", image: "adventure"),
                                 OurCateg(name: "Bluffing", image: "bluffing"),
                                 OurCateg(name: "Children Games", image: "children_game"),
                                 OurCateg(name: "Comic Book/Movies/TV", image: "comicbook_tv_series"),
                                 OurCateg(name: "Communication Limits", image: "communication_limits"),
                                 OurCateg(name: "Deduction/Murder Mystery", image: "deduction_murder_mystery"),
                                 OurCateg(name: "Dexterity", image: "dexterity"),
                                 OurCateg(name: "Escape Game", image: "escape_game"),
                                 OurCateg(name: "Exploration", image: "exploration"),
                                 OurCateg(name: "Family Game", image: "family_game"),
                                 OurCateg(name: "Fantasy", image: "fantasy"),
                                 OurCateg(name: "Fighting", image: "fighting"),
                                 OurCateg(name: "Horror", image: "horror"),
                                 OurCateg(name: "Memory", image: "memory"),
                                 OurCateg(name: "Party Game", image: "party_game"),
                                 OurCateg(name: "Resource Management", image: "resource_management"),
                                 OurCateg(name: "Sci-Fi", image: "sci-fi"),
                                 OurCateg(name: "Territory Building", image: "territory_building"),
                                 OurCateg(name: "Videogames", image: "videogames"),
                                 OurCateg(name: "War", image: "war")]
let componentsStrings: [OurCateg] = [OurCateg(name: "Cards", image: "card"),
                                     OurCateg(name: "Dice", image: "dice"),
                                     OurCateg(name: "Miniatures", image: "miniatures"),
                                     OurCateg(name: "Tiles", image: "tiles")]


func longName(_ name: String) -> (title: String, subtit: String)? {
    if name.contains(" - ") {
        let split = name.components(separatedBy: " - ")
        return (split[0], split[1])
    } else if name.contains(":") {
        let split = name.components(separatedBy: ": ")
        return (split[0], split[1])
    }
    return nil
}

func playtimeString(minPlaytime: Int?, maxPlaytime: Int?) -> String {
    if let maxPlaytime = maxPlaytime {
        switch maxPlaytime {
            case 0...10:
                return "up to 10'"
            case 11...30:
                return "up to 30'"
            case 31...60:
                return "up to 60'"
            case 61...90:
                return "up to 90'"
            case 91...:
                return "90'+"
            default:
                return "\(maxPlaytime) min"
        }
    } else if let minPlaytime = minPlaytime {
        switch minPlaytime {
            case 0...10:
                return "10 min"
            case 11...30:
                return "30 min"
            case 31...60:
                return "60 min"
            case 61...90:
                return "90 min"
            case 91...:
                return "90+ min"
            default:
                return "\(minPlaytime)"
        }
    }
    return "NA"
}

func playtimeStringShort(minPlaytime: Int?, maxPlaytime: Int?) -> String {
    if let maxPlaytime = maxPlaytime {
        switch maxPlaytime {
            case 0...10:
                return "<10'"
            case 11...30:
                return "<30'"
            case 31...60:
                return "<60'"
            case 61...90:
                return "<90'"
            case 91...:
                return "90'+"
            default:
                return "<\(maxPlaytime)'"
        }
    } else if let minPlaytime = minPlaytime {
        switch minPlaytime {
            case 0...10:
                return "10'+"
            case 11...30:
                return "30'+"
            case 31...60:
                return "60'+"
            case 61...:
                return "90'+"
            default:
                return "\(minPlaytime)'+"
        }
    }
    return "NA"
}

func playersString(minPlayers: Int?, maxPlayers: Int?) -> String {
    if let minPlayers = minPlayers, let maxPlayers = maxPlayers {
        if maxPlayers == 1 {
            return "Solo"
        }
        if maxPlayers == 9 {
            return "\(minPlayers) - 9+"
        }
        return "\(minPlayers) - \(maxPlayers)"
    }
    return "NA"
}

func playersStringShort(minPlayers: Int?, maxPlayers: Int?) -> String {
    if let minPlayers = minPlayers, let maxPlayers = maxPlayers {
        if maxPlayers == 1 {
            return "solo"
        }
        if maxPlayers == 9 {
            return "\(minPlayers)-9+"
        }
        return "\(minPlayers)-\(maxPlayers)"
    }
    return "NA"
}

func minAgeString(minAge: Int?) -> String {
    if let minAge = minAge {
        return "\(minAge)+"
    }
    return "NA"
}

func getTags(_ game: GameDataAPI?) -> String? {
    var tags: String = ""
    
    if let game = game {
        for category in game.categories {
            if let aTag = codeCategory(category.id) {
                if tags == "" {
                    tags = aTag
                } else {
                    tags = tags + ", " + aTag
                }
            }
        }
        for mechanic in game.mechanics {
            if let aTag = codeMechanics(mechanic.id) {
                if tags == "" {
                    tags = aTag
                } else {
                    tags = tags + ", " + aTag
                }
            }
        }
        if tags != "" { return tags }
    }
    
    return nil
}


extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
