//
//  FiltersView.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 18/02/22.
//

import SwiftUI

struct FiltersView: View {
    @Environment(\.dismiss) var dismiss
    @Binding var currentQuery: QueryGame
    @State var section = 0
    @State var isLocal: Bool = false
    @State var numPlayers = 0.0
    @State var isEditing = false
    @State var time = TimeIntervals.notSelected
    @State private var genreSelection = Array(repeating: false, count: genresStrings.count)
    @State private var componentsSelection = Array(repeating: false, count: componentsStrings.count)
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .bottom){
                Rectangle()
                    .foregroundColor(Color("DarkerBackground"))
                    .ignoresSafeArea()
                    VStack {
                        headerSegmentedControl
                            .padding()
                        if section == 0 {
                            Spacer()
                            playersSelection
                            Spacer()
                            timeSelection
                            Spacer()
                            globalOrLocal
                            Spacer()
                            VStack{}.frame(height: UIScreen.main.bounds.height*0.12)
                        } else {
                            ScrollView(showsIndicators: false){
                                VStack(alignment: .leading){
                                    genresList
                                    componentsList
                                }
                                .frame(width: UIScreen.main.bounds.width*0.9)
                                VStack{}.frame(height: UIScreen.main.bounds.height*0.12)
                            }
                        }
                    }
                    .foregroundColor(Color("Secondary"))
                bottomButtons
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbar{
                ToolbarItem(id: "Done", placement: .principal, showsByDefault: true) {
                    doneButton
                }
            }
        }
    }
    
    
    @ViewBuilder
    var headerSegmentedControl: some View {
        Picker("Section", selection: $section) {
            Text("General").tag(0)
            Text("Categories").tag(1)
        }
        .pickerStyle(SegmentedPickerStyle())
        .frame(width: UIScreen.main.bounds.width*0.9)
    }
    
    @ViewBuilder
    var playersSelection: some View {
        VStack {
            Text("How many players?")
                .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title2))
                .fontWeight(.bold)
                .padding(.bottom)
            Text(LocalizedStringKey(numPlayers == 0 ? "Not Selected" : numPlayers == 10 ? "9+" : String(format: "%.0f", numPlayers)))
                .font(Font.custom("Commissioner-Thin_Regular", size: 18))
                .fontWeight(numPlayers == 0 ? .regular : .bold)
                .foregroundColor(isEditing ? Color("AccentColor") : Color("Secondary"))
            Slider(value: $numPlayers, in: 0...10, step: 1){
                Text("Players")
            } minimumValueLabel: {
                Text("\(Image(systemName: "person.2.fill"))")
            } maximumValueLabel: {
                Text("9+")
                    .font(Font.custom("Commissioner-Thin_Regular", size: 16))
            } onEditingChanged: { editing in
                isEditing = editing
            }
            .frame(width: UIScreen.main.bounds.width*0.85)
        }
    }
    
    @ViewBuilder
    var timeSelection: some View {
        VStack{
            Text("How much time do you have?")
                .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title2))
                .fontWeight(.bold)
            Picker("Time", selection: $time){
                ForEach(TimeIntervals.allCases, id: \.self) { interval in
                    Text(interval.rawValue).tag(interval)
                        .foregroundColor(Color("Secondary").opacity(1))
                        .font(Font.custom("Commissioner-Thin_Regular", size: 23))
                    .font(.title)                        }
            }
            .pickerStyle(WheelPickerStyle())
            .frame(height: UIScreen.main.bounds.height*0.14)
            .cornerRadius(10)
            .padding()
            .padding(.horizontal, UIScreen.main.bounds.width*0.3)
        }
    }
    
    @ViewBuilder
    var globalOrLocal: some View {
        VStack {
            Toggle(isOn: $isLocal) {
                Text("Search in my collection")
                    .font(Font.custom("Commissioner-Thin_Regular", size: 20))
                    .fontWeight(.semibold)
            }
            .tint(Color("AccentColor"))
            .padding()
        }
        .frame(width: UIScreen.main.bounds.width*0.9)
        .background(Color("Background"))
        .cornerRadius(10)
        .clipped()
    }
    
    @ViewBuilder
    var genresList: some View {
        Text("Genres")
            .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title))
            .fontWeight(.bold)
            .padding(.vertical)
        ForEach((0...genresStrings.count-1), id: \.self) { i in
            HStack{
                Image(genresStrings[i].image)
                    .frame(width: 25)
                    .padding(.trailing)
                Text(LocalizedStringKey(genresStrings[i].name))
                    .font(Font.custom("Commissioner-Thin_Regular", size: 18))
                    .fontWeight(.medium)
                Spacer()
                CheckBoxView(checked: $genreSelection[i])
            }
            .onTapGesture {
                self.genreSelection[i].toggle()
            }
            .padding(.bottom,10)
        }
    }
    
    @ViewBuilder
    var componentsList: some View {
        Text("Components")
            .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title))
            .fontWeight(.bold)
            .padding(.vertical)
            .padding(.top)
        ForEach((0...componentsStrings.count-1), id: \.self) { i in
            HStack{
                Image(componentsStrings[i].image)
                    .frame(width: 25)
                    .padding(.trailing)
                Text(LocalizedStringKey(componentsStrings[i].name))
                    .font(Font.custom("Commissioner-Thin_Regular", size: 18))
                    .fontWeight(.medium)
                Spacer()
                CheckBoxView(checked: $componentsSelection[i])
            }
            .onTapGesture {
                self.componentsSelection[i].toggle()
            }
            .padding(.bottom,10)
        }
    }
    
    @ViewBuilder
    var bottomButtons: some View {
        HStack {
            Button(action: {
                isLocal = false
                numPlayers = 0.0
                time = .notSelected
                genreSelection = Array(repeating: false, count: genresStrings.count)
                componentsSelection = Array(repeating: false, count: componentsStrings.count)
                currentQuery = QueryGame()
            }, label: {
                Text("Clear all")
                    .font(Font.custom("Commissioner-Thin_Regular", size: 16))
                    .fontWeight(.semibold)
            })
            .padding(.vertical, 12)
            .padding()
            Spacer()
            Button(action: {
                if numPlayers != 0.0 {
                    currentQuery.min_players = Int(numPlayers)
                }
                switch time {
                    case .upTo10:
                        currentQuery.lt_max_playtime = 11
                    case .upTo30:
                        currentQuery.lt_max_playtime = 31
                    case .upTo60:
                        currentQuery.lt_max_playtime = 61
                    case .upTo90:
                        currentQuery.lt_max_playtime = 91
                    case .moreThan90:
                        currentQuery.lt_max_playtime = 211
                    case .notSelected:
                        break
                }
                for index in 0...(genreSelection.count-1) {
                    if genreSelection[index] {
                        currentQuery = queryTagHelper(genresStrings[index].name, query: currentQuery)
                    }
                }
                for index in 0...(componentsSelection.count-1) {
                    if componentsSelection[index] {
                        currentQuery = queryTagHelper(componentsStrings[index].name, query: currentQuery)
                    }
                }
                dismiss()
            }, label: {
                Text("Show Results")
                    .font(Font.custom("Commissioner-Thin_Regular", size: 16))
                    .fontWeight(.semibold)
                    .padding(.vertical, 10)
                    .padding(.horizontal)
            })
                .buttonStyle(.borderedProminent)
        }
        .padding(.horizontal)
        .padding(.bottom)
        .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height*0.1)
        .background(Color("DarkerBackground"))
    }
    
    @ViewBuilder
    var doneButton: some View {
        HStack{
            Button(action: {
                dismiss()
            }, label: {
                RoundedRectangle(cornerRadius: 10)
                    .frame(width: 70, height: 5)
                    .foregroundColor(Color.gray).opacity(0.4)
            })
        }
        .padding()
    }
}

//struct ShowMoreGames_Previews: PreviewProvider {
//    static var previews: some View {
//        FiltersView()
//    }
//}
