//
//  OnboardingView.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 07/03/22.
//

import SwiftUI

struct OnboardingView: View {
    @Environment(\.dismiss) var dismiss
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(entity: GameData.entity(), sortDescriptors: []) var gameData: FetchedResults<GameData>
    @FetchRequest(entity: ImageData.entity(), sortDescriptors: []) var imageData: FetchedResults<ImageData>
    
    @State private var isSearching = false
    @State private var secondPage = false
    @State var gameList: [GameDataAPI]?
    @State private var gameSelection = [false]
    @State private var queryForGame = QueryGame()
    @State private var gamesToAdd: [GameDataAPI]? = nil
    @Binding var showOnboarding: Bool
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .top) {
                Rectangle()
                    .foregroundColor(Color("DarkerBackground"))
                    .ignoresSafeArea()
                if secondPage {
                    VStack {
                        Text("create-collection")
                            .foregroundColor(Color("Secondary"))
                            .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title))
                            .fontWeight(.bold)
                            .padding(.horizontal)
                            .padding(10)
                            .multilineTextAlignment(.center)
                        Text("select-games")
                            .font(Font.custom("Commissioner-Thin_Regular", size: 16))
                            .foregroundColor(Color("Secondary"))
                        SearchBar(searchText: $queryForGame.name, searching: $isSearching)
                            .frame(width: UIScreen.main.bounds.width*0.9)
                        ScrollViewReader { scrollReader in
                            ScrollView(showsIndicators: false){
                                VStack {
                                    if gameList != nil {
                                        ForEach((0...gameList!.endIndex-1), id: \.self) { index in
                                            Button(action: {
                                                if gameSelection[index] == true {
                                                    gamesToAdd = gamesToAdd?.filter{ $0.id != gameList![index].id }
                                                } else {
                                                    if gamesToAdd != nil {
                                                        gamesToAdd?.append(gameList![index])
                                                    } else {
                                                        gamesToAdd = [gameList![index]]
                                                    }
                                                }
                                                gameSelection[index].toggle()
                                            }, label: {
                                                ListElementOnboarding(game: gameList![index], isLast: gameList![index] == gameList!.last, isSelected: $gameSelection[index])
                                                    .frame(width: UIScreen.main.bounds.width*0.85)
                                            })
                                        }
                                    } else {
                                        Text("Loading...")
                                            .font(Font.custom("Commissioner-Thin_Regular", size: 16))
                                            .foregroundColor(Color("SecondaryBis"))
                                    }
                                }
                                .frame(width: UIScreen.main.bounds.width*0.9)
                                .background(.white)
                                .cornerRadius(10)
                                .clipped()
                                .shadow(color: Color("Secondary").opacity(0.2), radius: 5, x: 0, y: 0)
                                .padding()
                                VStack{}.frame(height: UIScreen.main.bounds.height*0.12)
                            }
                            .frame(width: UIScreen.main.bounds.width)
                            .onChange(of: gameList){ _ in
                                withAnimation { scrollReader.scrollTo(0) }
                            }
                        }
                    }
                    .frame(width: UIScreen.main.bounds.width*0.9)
                } else {
                    VStack {
                        Image("Onboarding")
                            .resizable()
                            .scaledToFill()
                            .ignoresSafeArea()
                            .frame(height: UIScreen.main.bounds.height*0.4)
                        Spacer()
                    }
                    VStack(){
                        Rectangle()
                            .foregroundColor(.clear)
                            .frame(height: UIScreen.main.bounds.height*0.48)
                        VStack(alignment: .leading){
                            Text("ready?")
                                .foregroundColor(Color("Secondary"))
                                .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title))
                                .fontWeight(.bold)
                                .padding(.vertical)
                            Text("Wari").font(Font.custom("Commissioner-Thin_Regular", size: 16)).foregroundColor(Color("AccentColor")) + Text("helps you").font(Font.custom("Commissioner-Thin_Regular", size: 16)).foregroundColor(Color("Secondary")) + Text("board games").font(Font.custom("Commissioner-Thin_Regular", size: 16)).foregroundColor(Color("AccentColor")) + Text(".").font(Font.custom("Commissioner-Thin_Regular", size: 16)).foregroundColor(Color("Secondary"))
                        }
                        .frame(width: UIScreen.main.bounds.width*0.8, alignment: .leading)
                        Button(action: {
                            secondPage = true
                        }, label: {
                            Text("start")
                                .font(Font.custom("Commissioner-Thin_Regular", size: 16))
                                .padding()
                        })
                            .buttonStyle(.borderedProminent)
                            .padding(.vertical)
                            .padding(.top)
                    }
                }
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                if secondPage {
                    nextButton
                }
            }
        }
        .onAppear {
            Task {
                do {
                    let games = try await getPopular()
                    guard let games = games else { return }
                    
                    gameList = games
                    gameSelection = Array(repeating: false, count: gameList?.count ?? 0)
                } catch {
                    print(error)
                }
            }
        }
        .onChange(of: queryForGame) { terms in
            Task {
                do {
                    let query = createQuery(terms)
                    let search = try await getGameByQuery(query)
                    guard let search = search else { return }
                    
                    gameList = search
                    gameSelection = Array(repeating: false, count: gameList?.count ?? 0)
                    
                    if gamesToAdd != nil {
                        for index in (0...gameList!.count-1) {
                            if gamesToAdd!.contains(gameList![index]) {
                                gameSelection[index] = true
                            }
                        }
                    }
                } catch {
                    print(error)
                }
            }
        }
    }
    
    @ViewBuilder
    var nextButton: some View {
        HStack{
            Button(action: {
                if gamesToAdd != nil {
                    gamesToAdd = gamesToAdd?.removingDuplicates()
                    for gameToAdd in gamesToAdd! {
                        var gameImage = ImageData(context: managedObjectContext)
                        gameImage = createImageCopy(gameToAdd, in: gameImage)

                        var game = GameData(context: managedObjectContext)
                        game = createGameCopy(gameToAdd, in: game)

                        gameImage.isAnImageOf = game
                        game.collection = 0
                    }
                }
                do {
                    try managedObjectContext.save()
                } catch {
                    let nsError = error as NSError
                    fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                }
                showOnboarding = false
                dismiss()
            }, label: {
                Text(gamesToAdd == nil ? "Skip" : "Next").font(Font.custom("Commissioner-Thin_Regular", size: 16))
            })
                .buttonStyle(.borderless)
        }
        .padding()
    }
}

struct ListElementOnboarding: View {
    var game: GameDataAPI
    var isLast: Bool = false
    @Binding var isSelected: Bool
    
    var body: some View {
        VStack (alignment: .leading){
            HStack {
                AsyncImage(url: game.getImageURL(),
                           content: { image in
                    image
                        .resizable()
                        .scaledToFill()
                        .scaledToFit()
                        .padding()
                }, placeholder: {
                    ProgressView()
                })
                    .frame(width: UIScreen.main.bounds.width*0.25, height: UIScreen.main.bounds.height*0.1)
                VStack(alignment: .leading){
                    Text(game.name)
                        .font(Font.custom("Commissioner-Thin_Regular", size: 18, relativeTo: .title3))
                        .fontWeight(.semibold)
                        .foregroundColor(Color("Secondary"))
                    Spacer()
                    Text("time: ").font(Font.custom("Commissioner-Thin_Regular", size: 16)).fontWeight(.light).foregroundColor(Color("Secondary")) + Text(playtimeStringShort(minPlaytime: game.min_playtime, maxPlaytime: game.max_playtime)).font(Font.custom("Commissioner-Thin_Regular", size: 16)).fontWeight(.medium).foregroundColor(Color("AccentColor")) + Text("   ") + Text("players: ").font(Font.custom("Commissioner-Thin_Regular", size: 16)).fontWeight(.light).foregroundColor(Color("Secondary")) + Text(playersStringShort(minPlayers: game.min_players, maxPlayers: game.max_players)).font(Font.custom("Commissioner-Thin_Regular", size: 16)).fontWeight(.medium).foregroundColor(Color("AccentColor"))
                        
                }
                .font(.subheadline)
                .frame(height: UIScreen.main.bounds.height*0.06)
                .multilineTextAlignment(.leading)
                Spacer()
                CheckBoxView(checked: $isSelected)
                    .padding(.trailing)
            }
            if !isLast { Divider() }
        }
    }
}


func createImageCopy(_ data: GameDataAPI?, in gameImage: ImageData) -> ImageData {
    gameImage.id = UUID()
    gameImage.large = data?.images.large
    gameImage.medium = data?.images.medium
    gameImage.original = data?.images.original
    gameImage.small = data?.images.small
    gameImage.thumb = data?.images.thumb
    
    return gameImage
}

func createGameCopy(_ data: GameDataAPI?, in game: GameData) -> GameData {
    game.idGame = data?.id
    game.averageUserRating = data?.average_user_rating ?? Double.leastNonzeroMagnitude
    game.name = data?.name
    game.maxPlaytime = Int32(data?.max_playtime ?? Int(Int32.min))
    game.minPlaytime = Int32(data?.min_playtime ?? Int(Int32.min))
    game.maxPlayers = Int32(data?.min_playtime ?? Int(Int32.min))
    game.minPlayers = Int32(data?.max_players ?? Int(Int32.min))
    game.minAge = Int32(data?.min_age ?? Int(Int32.min))
    game.gameDescription = data?.description
    
    return game
    }
