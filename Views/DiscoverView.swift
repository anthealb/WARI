//
//  DiscoverView.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 15/02/22.
//

// MAKE SEARCH WORK

import SwiftUI

struct DiscoverView: View {
    @State private var showingFilters = false
    @State private var gameList: [GameDataAPI]? = nil
    @Binding var queryForGame: QueryGame
    @Binding var isSearching: Bool
    @Binding var kickstarters: [GameDataAPI]?
    @Binding var populars: [GameDataAPI]?
    
    var body: some View {
            ZStack{
                Rectangle()
                    .foregroundColor(Color("DarkerBackground"))
                    .ignoresSafeArea()
                VStack {
                    HStack {
                        SearchBar(searchText: $queryForGame.name, searching: $isSearching)
                        filterButton
                    }
                    .frame(width: UIScreen.main.bounds.width*0.9)
                        .padding(.horizontal)
                    ScrollView(showsIndicators: false){
                        if queryForGame.isEmpty() {
                            NavigationLink(destination: {
                                TestView()
                            }, label: {
                                gamesForYou
                            })
                                .padding(.vertical)
                            HScrollBoxesElement(title: "Popular Games", games: $populars)
                                .padding(.vertical)
                            HScrollBoxesElement(title: "Live Kickstarters", games: $kickstarters)
                                .padding(.bottom)
                            categoriesList
                            VStack{}.frame(width: nil, height: 50)
                        } else {
                            resultsView
                        }
                    }
                }
            }
            .toolbar{
                ToolbarItem(placement: .navigation) {
                    Button(action: {
                        withAnimation{
                            queryForGame = QueryGame()
                        }
                    }, label: {
                        if !queryForGame.isEmpty() {
                            HStack{
                                Image(systemName: "chevron.left")
                                Text("Back")
                                    .font(Font.custom("Commissioner-Thin_Regular", size: 17))
                            }
                        }
                    })
                }
            }
    }
    
    @ViewBuilder
    var gamesForYou: some View {
        ZStack(alignment: .bottom) {
            Rectangle()
                .foregroundColor(Color.white)
            VStack {
                Image("Recommended")
                    .resizable()
                    .scaledToFill()
                    .frame(width: UIScreen.main.bounds.width*0.9, height: UIScreen.main.bounds.height*0.28, alignment: .trailing)
                Spacer()
            }
            Text("Recommended for you")
                .font(Font.custom("Fraunces-Bold", size: 27, relativeTo: .title))
                .fontWeight(.bold)
                .foregroundColor(Color("Secondary"))
                .padding(.bottom)
        }
        .frame(width: UIScreen.main.bounds.width*0.9, height: UIScreen.main.bounds.height*0.32)
        .foregroundColor(Color.white)
        .cornerRadius(10)
        .clipped()
        .shadow(color: Color("Secondary").opacity(0.5), radius: 7, x: 4, y: 4)
    }
    
    @ViewBuilder
    var categoriesList: some View {
        HStack {
            Text("Categories")
                .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title))
                .fontWeight(.bold)
                .foregroundColor(Color("Secondary"))
            Spacer()
            NavigationLink(destination: {
                ShowAllCategoriesView()
            }, label: {
                Text("SEE ALL")
                    .font(Font.custom("Commissioner-Thin_Regular", size: 13, relativeTo: .callout).smallCaps())
                    .fontWeight(.semibold)
                    .padding(.top, 5)
            })
        }
        .padding(.bottom, 1)
        .frame(width: UIScreen.main.bounds.width*0.9)
        VStack(alignment: .leading){
            NavigationLink(destination: {
                CategoryView(gamesArr: $gameList, searchTerms: $queryForGame, title: "Party Game")
                    .onAppear(perform: {
                        Task {
                            do {
                                let query = createQuery(queryTagHelper("Party Game", query: nil))
                                let thisCateg = try await getGameByQuery(query)
                                guard let thisCateg = thisCateg else { return }
                                
                                gameList = thisCateg
                            } catch {
                                print(error)
                            }
                        }
                    })
                    .onDisappear(perform: { gameList = nil })
            }, label: {
                ListElementDiscovery(image: "party_game", category: "Party Game")
            })
                .padding(.top, 8)
            NavigationLink(destination: {
                CategoryView(gamesArr: $gameList, searchTerms: $queryForGame, title: "Family Game")
                    .onAppear(perform: {
                        Task {
                            do {
                                let query = createQuery(queryTagHelper("Family Game", query: nil))
                                let thisCateg = try await getGameByQuery(query)
                                guard let thisCateg = thisCateg else { return }
                                
                                gameList = thisCateg
                            } catch {
                                print(error)
                            }
                        }
                    })
                    .onDisappear(perform: { gameList = nil })
            }, label: {
                ListElementDiscovery(image: "family_game", category: "Family Game")
            })
                .padding(.top, 8)
            NavigationLink(destination: {
                CategoryView(gamesArr: $gameList, searchTerms: $queryForGame, title: "Cards")
                    .onAppear(perform: {
                        Task {
                            do {
                                let query = createQuery(queryTagHelper("Cards", query: nil))
                                let thisCateg = try await getGameByQuery(query)
                                guard let thisCateg = thisCateg else { return }
                                
                                gameList = thisCateg
                            } catch {
                                print(error)
                            }
                        }
                    })
                    .onDisappear(perform: { gameList = nil })
            }, label: {
                ListElementDiscovery(image: "card", category: "Cards")
            })
                .padding(.top, 8)
            NavigationLink(destination: {
                CategoryView(gamesArr: $gameList, searchTerms: $queryForGame, title: "Dice")
                    .onAppear(perform: {
                        Task {
                            do {
                                let query = createQuery(queryTagHelper("Dice", query: nil))
                                let thisCateg = try await getGameByQuery(query)
                                guard let thisCateg = thisCateg else { return }
                                
                                gameList = thisCateg
                            } catch {
                                print(error)
                            }
                        }
                    })
                    .onDisappear(perform: { gameList = nil })
            }, label: {
                ListElementDiscovery(image: "dice", category: "Dice")
            })
                .padding(.top, 8)
            NavigationLink(destination: {
                CategoryView(gamesArr: $gameList, searchTerms: $queryForGame, title: "Bluffing")
                    .onAppear(perform: {
                        Task {
                            do {
                                let query = createQuery(queryTagHelper("Bluffing", query: nil))
                                let thisCateg = try await getGameByQuery(query)
                                guard let thisCateg = thisCateg else { return }
                                
                                gameList = thisCateg
                            } catch {
                                print(error)
                            }
                        }
                    })
                    .onDisappear(perform: { gameList = nil })
            }, label: {
                ListElementDiscovery(image: "bluffing", category: "Bluffing")
            })
                .padding(.top, 8)
        }
    }
    
    @ViewBuilder
    var filterButton: some View {
            Button(action: {
                showingFilters.toggle()
            }, label: {
                Image(systemName: "line.3.horizontal.decrease.circle")
                    .font(.title2.bold())
            })
                .sheet(isPresented: $showingFilters, content: {
                    FiltersView(currentQuery: $queryForGame)
                })
                .padding(.leading)
    }
    
    var resultsView: some View {
        ResultsView(gamesArr: $gameList, queryForGame: $queryForGame, isSearching: $isSearching, title: "Search")
            .onAppear(perform: {
                Task {
                    do {
                        let query = createQuery(queryForGame)
                        let thisSearch = try await getGameByQuery(query)
                        guard let thisSearch = thisSearch else { return }
                        
                        gameList = thisSearch
                    } catch {
                        print(error)
                    }
                }
            })
    }
    
}

//struct DiscoverView_Previews: PreviewProvider {
//    static var previews: some View {
//        DiscoverView()
//    }
//}
