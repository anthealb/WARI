//
//  ContentView.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 06/02/22.
// 
import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    init () {
        UITextField.appearance().backgroundColor = UIColor.clear
        UISearchBar.appearance().barTintColor = UIColor.clear
        UISearchBar.appearance().searchBarStyle = .minimal
        UITextField.appearance().textColor = UIColor.secondary()
        UITextField.appearance().tintColor = UIColor.secondary()
        
        UITableViewCell.appearance().backgroundColor = UIColor.background()
        UITableView.appearance().backgroundColor = UIColor.background()
        UISegmentedControl.appearance().selectedSegmentTintColor = UIColor.accent()
        UISegmentedControl.appearance().tintAdjustmentMode = UIView.TintAdjustmentMode.normal
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white, .font : UIFont.init(name: "Commissioner-Thin_Regular", size: 14)!], for: .selected)
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.secondary(), .font : UIFont.init(name: "Commissioner-Thin_Regular", size: 14)!], for: .normal)
        
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.secondary(), .font : UIFont.init(name: "Commissioner-SemiBold", size: 34)!]
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.secondary(), .font : UIFont.init(name: "Commissioner-Semibold", size: 17)!]
        UIBarButtonItem.appearance().setTitleTextAttributes([.font : UIFont.init(name: "Commissioner-Thin_Regular", size: 17)!], for: .normal)
        UITabBar.appearance().tintAdjustmentMode = UIView.TintAdjustmentMode.normal
        UITabBar.appearance().unselectedItemTintColor = UIColor.secondaryBis()
        UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = UIColor.accent()
    }
    @State private var searchTerms = QueryGame()
    @State private var kickstarters: [GameDataAPI]?
    @State private var populars: [GameDataAPI]?
    @State private var isSearching = false
    @AppStorage("onboarding") var showOnboarding = true
    
    var body: some View{
        TabView{
            discoveryView
                .tabItem{
                    Label("Discovery", systemImage: "magnifyingglass")
                }
                
            collectionView
                .tabItem{
                    Label("Collection", systemImage: "rectangle.stack.fill")
                }
        }
        .fullScreenCover(isPresented: $showOnboarding, content: {
            OnboardingView(gameList: populars, showOnboarding: $showOnboarding)
        })
        .onAppear {
            Task {
                do {
                    let popList = try await getPopular()
                    guard let popList = popList else { return }
                    
                    let kickList = try await getKickstarters()
                    guard let kickList = kickList else { return }
                    
                    kickstarters = kickList
                    populars = popList
                } catch {
                    print(error)
                }
            }
        }
        .gesture(DragGesture()
                    .onChanged({ _ in
            UIApplication.shared.dismissKeyboard()
            withAnimation {
                isSearching = false
            }
        }))
    }
}

//Nav Views
private extension ContentView {
    var discoveryView: some View {
        NavigationView {
            DiscoverView(queryForGame: $searchTerms, isSearching: $isSearching, kickstarters: $kickstarters, populars: $populars)
                .navigationBarTitleDisplayMode(.inline)
        }
    }
    var collectionView: some View {
        NavigationView {
            CollectionView()
                .navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct TestView: View {
    var body: some View {
        ZStack {
            Rectangle()
                .foregroundColor(Color("DarkerBackground"))
            Text("Work in progress")
                .font(Font.custom("Commissioner-Thin_Regular", size: 16))
                .foregroundColor(Color("Secondary"))
        }
    }
}

extension UIApplication {
    func dismissKeyboard() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
