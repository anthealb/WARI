//
//  ProgressBar.swift
//  WARI
//
//  Created by Roberta Ruggiero on 06/03/22.
//


import SwiftUI

//Esempio: ProgressBar(value: $progressValue).frame(height: 20)
//Dove $progressValue deve essere il valore della percentuale
//(passata per la funzione di controllo rapporto)
//trasformata in Float e diviso 100.0

struct ProgressBar: View {
    @Binding var value: Float
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                Rectangle().frame(width: geometry.size.width , height: geometry.size.height)
                    .opacity(0.3)
                    .foregroundColor(Color(UIColor.systemTeal))
                
                Rectangle().frame(width: min(CGFloat(self.value)*geometry.size.width, geometry.size.width), height: geometry.size.height)
                    .foregroundColor(Color(UIColor.systemOrange))
//                    .animation(.linear)
            }.cornerRadius(45.0)
        }
    }
}
