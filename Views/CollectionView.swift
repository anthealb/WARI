//
//  CollectionView.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 28/02/22.
//

import SwiftUI

struct CollectionView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(entity: GameData.entity(), sortDescriptors: []) var games: FetchedResults<GameData>
    @State private var section = 0
    private var gridLayout = Array(repeating: GridItem(.flexible()), count: 2)
    @State private var owned: Array<GameData> = []
    @State private var wishlist: Array<GameData> = []
    
    @State private var isSearching = false
    @State private var query = ""
    
    var body: some View {
        ZStack(alignment: .top){
            Rectangle()
                .foregroundColor(Color("DarkerBackground"))
                .ignoresSafeArea()
            VStack {
                SearchBar(searchText: $query, searching: $isSearching)
                    .frame(width: UIScreen.main.bounds.width*0.9)
                headerSegmentedControl
                    .padding(.horizontal)
                    .padding(.bottom)
                ScrollView(showsIndicators:false) {
                    if section == 0 {
                        if owned.count > 0 {
                            LazyVGrid(columns: gridLayout){
                                ForEach(owned, id: \.self) { game in
                                    NavigationLink(destination: {
                                        DetailView(thisGame: GameDataAPI(from: game))
                                    }, label: {
                                        CollectionBox(thisGame: game)
                                            .contextMenu{
                                                Button(action: {
                                                    game.collection = 1
                                                    
                                                    do {
                                                        try managedObjectContext.save()
                                                    } catch {
                                                        let nsError = error as NSError
                                                        fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                                                    }
                                                    owned = games.filter {$0.collection == 0}
                                                    wishlist = games.filter {$0.collection == 1}
                                                },     label: {
                                                    Label("Move to Wishlist", systemImage: "rectangle.stack")
                                                })
//                                                Button(action: {
//                                                    //
//                                                },     label: {
//                                                    Label("Share", systemImage: "square.and.arrow.up")
//                                                })
                                                Button(role: .destructive,
                                                       action: {
                                                    //                                                            managedObjectContext.delete(game.getImage)
                                                    //                                                            managedObjectContext.delete(game)
                                                    //
                                                    //                                                            do {
                                                    //                                                                try managedObjectContext.save()
                                                    //                                                            } catch {
                                                    //                                                                let nsError = error as NSError
                                                    //                                                                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                                                    //                                                            }
                                                    //                                                            owned = games.filter {$0.collection == 0}
                                                    //                                                            wishlist = games.filter {$0.collection == 1}
                                                },     label: {
                                                    Label("Delete", systemImage: "trash.fill")
                                                })
                                            }
                                            .padding(.vertical, 10)
                                            .shadow(color: Color("Secondary").opacity(0.25), radius: 7, x: 4, y: 4)
                                    })
                                        .buttonStyle(FlatLinkStyle())
                                }
                            }
                            .padding(.horizontal)
                        } else {
                            collectionIsEmpty
                        }
                    } else {
                        if wishlist.count > 0 {
                            LazyVGrid(columns: gridLayout){
                                ForEach(wishlist, id: \.self) { game in
                                    NavigationLink(destination: {
                                        DetailView(thisGame: GameDataAPI(from: game))
                                    }, label: {
                                        CollectionBox(thisGame: game)
                                            .contextMenu{
                                                Button(action: {
                                                    game.collection = 0
                                                    
                                                    do {
                                                        try managedObjectContext.save()
                                                    } catch {
                                                        let nsError = error as NSError
                                                        fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                                                    }
                                                    owned = games.filter {$0.collection == 0}
                                                    wishlist = games.filter {$0.collection == 1}
                                                },     label: {
                                                    Label("Move to Owned", systemImage: "rectangle.stack")
                                                })
//                                                Button(action: {
//                                                    //
//                                                },     label: {
//                                                    Label("Share", systemImage: "square.and.arrow.up")
//                                                })
                                                Button(role: .destructive,
                                                       action: {
                                                    //                                                            managedObjectContext.delete(game.getImage)
                                                    //                                                            managedObjectContext.delete(game)
                                                },     label: {
                                                    Label("Delete", systemImage: "trash.fill")
                                                })
                                            }
                                            .padding(.vertical, 10)
                                            .shadow(color: Color("Secondary").opacity(0.25), radius: 7, x: 4, y: 4)
                                    })
                                        .buttonStyle(FlatLinkStyle())
                                }
                            }
                            .padding(.horizontal)
                        } else {
                            collectionIsEmpty
                        }
                    }
                    VStack{}.frame(height: UIScreen.main.bounds.height*0.12)
                }
                .onAppear{
                    owned = games.filter {$0.collection == 0}
                    wishlist = games.filter {$0.collection == 1}
                }
                .onChange(of: query) { filter in
                    if !filter.isEmpty{
                        owned = owned.filter{ $0.getName.contains(filter) }
                        wishlist = wishlist.filter{ $0.getName.contains(filter) }
                    } else {
                        owned = games.filter {$0.collection == 0}
                        wishlist = games.filter {$0.collection == 1}
                    }
                }
            }
            .foregroundColor(Color("Secondary"))
        }
    }
    
    @ViewBuilder
    var headerSegmentedControl: some View {
        Picker("Section", selection: $section) {
            Text("Owned").tag(0)
            Text("Wishlist").tag(1)
        }
        .pickerStyle(SegmentedPickerStyle())
        .frame(width: UIScreen.main.bounds.width*0.9)
    }
    
    @ViewBuilder
    var collectionIsEmpty: some View {
        Text("This collection is currently empty")
            .font(Font.custom("Commissioner-Thin_Regular", size: 16))
            .foregroundColor(Color("SecondaryBis"))
            .padding()
            .frame(width: UIScreen.main.bounds.width*0.9)
            .background(.white)
            .cornerRadius(10)
            .shadow(color: Color("Secondary").opacity(0.25), radius: 7, x: 4, y: 4)
            .padding()
    }
    
}

struct CollectionBox: View {
    @ObservedObject var thisGame: GameData
    
    var body: some View {
        ZStack(alignment: .bottom){
            VStack {
                AsyncImage(url: getURL(thisGame.getImage), content: { image in
                    image
                        .resizable()
                        .scaledToFill()
                        .scaledToFit()
                        .padding()
                }, placeholder: {
                    ProgressView()
                })
                    .frame(width: UIScreen.main.bounds.width*0.4, height: UIScreen.main.bounds.width*0.35)
                Spacer()
            }
            .background(Color.white)
            .frame(width: UIScreen.main.bounds.width*0.4, height: UIScreen.main.bounds.width*0.45)
            ZStack(){
                Text(thisGame.getName)
                    .font(Font.custom("Commissioner-Thin_Regular", size: 16))
                    .fontWeight(.semibold)
                    .multilineTextAlignment(.leading)
                    .foregroundColor(Color("Secondary"))
                    .padding(.horizontal, 10)
            }
            .padding(.vertical,5)
            .frame(width: UIScreen.main.bounds.width*0.4, height: UIScreen.main.bounds.width*0.12, alignment: .topLeading)
            .background(Color.white)
        }
        .cornerRadius(10)
        .clipped()
    }
    
}

func getURL(_ img: ImageData?) -> URL {
    if img != nil {
        if img?.getOriginal == "https://s3-us-west-1.amazonaws.com/5cc.images/games/empty+box.jpg" {
            return URL(string: defaultImg)!
        }
        return URL(string: img!.getSmall)!
    }
    return URL(string: defaultImg)!
}

struct FlatLinkStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
    }
}
//struct CollectionView_Previews: PreviewProvider {
//    static var previews: some View {
//        CollectionView()
//    }
//}
