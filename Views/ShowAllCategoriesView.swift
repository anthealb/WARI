//
//  ShowMoreCategoriesView.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 17/02/22.
//

import SwiftUI

struct ShowAllCategoriesView: View {
    @State private var segment = 0
    @State private var gameList: [GameDataAPI]?
    @State private var emptyQuery = QueryGame()
    
    var body: some View {
        ZStack {
            Rectangle()
                .foregroundColor(Color("DarkerBackground"))
                .ignoresSafeArea()
            ScrollView{
                Text("Genres")
                    .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title))
                    .fontWeight(.bold)
                    .frame(minWidth: 0, maxWidth: UIScreen.main.bounds.width*0.9, alignment: .leading)
                VStack {
                    ForEach(genresStrings, id: \.self) { genre in
                        NavigationLink(destination: {
                            CategoryView(gamesArr: $gameList, searchTerms: $emptyQuery, title: LocalizedStringKey(genre.name))
                                .onAppear(perform: {
                                    Task {
                                        do {
                                            let query = createQuery(queryTagHelper(genre.name, query: nil))
                                            let thisCateg = try await getGameByQuery(query)
                                            guard let thisCateg = thisCateg else { return }
                                            
                                            gameList = thisCateg
                                        } catch {
                                            print(error)
                                        }
                                    }
                                })
                                .onDisappear(perform: { gameList = nil })
                        }, label: {
                            ListElement(image: genre.image, category: LocalizedStringKey(genre.name), isLast: genre == genresStrings.last)
                        })
                    }
                }
                .padding()
                .frame(width: UIScreen.main.bounds.width*0.9)
                .background(Color.white)
                .cornerRadius(10)
                .clipped()
                Text("Components")
                    .font(Font.custom("Fraunces-Bold", size: 24, relativeTo: .title))
                    .fontWeight(.bold)
                    .frame(minWidth: 0, maxWidth: UIScreen.main.bounds.width*0.9, alignment: .leading)
                    .padding(.top)
                VStack {
                    ForEach(componentsStrings, id: \.self) { comp in
                        NavigationLink(destination: {
                            CategoryView(gamesArr: $gameList, searchTerms: $emptyQuery, title: LocalizedStringKey(comp.name))
                                .onAppear(perform: {
                                    Task {
                                        do {
                                            let query = createQuery(queryTagHelper(comp.name, query: nil))
                                            let thisCateg = try await getGameByQuery(query)
                                            guard let thisCateg = thisCateg else { return }
                                            
                                            gameList = thisCateg
                                        } catch {
                                            print(error)
                                        }
                                    }
                                })
                                .onDisappear(perform: { gameList = nil })
                        }, label: {
                            ListElement(image: comp.image, category: LocalizedStringKey(comp.name), isLast: comp == componentsStrings.last)
                        })
                    }
                }
                .padding()
                .frame(width: UIScreen.main.bounds.width*0.9)
                .background(Color.white)
                .cornerRadius(10)
                .clipped()
                VStack{}.frame(width: nil, height: 50)
            }
            .padding(.vertical)
        }
        .foregroundColor(Color("Secondary"))
        .navigationBarTitle("Categories", displayMode: .inline)
    }
}

//struct ShowMoreCategoriesView_Previews: PreviewProvider {
//    static var previews: some View {
//        ShowAllCategoriesView()
//    }
//}
