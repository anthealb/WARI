//
//  KickStarterPart.swift
//  WARI
//
//  Created by Roberta Ruggiero on 06/03/22.
//

import SwiftUI
import Foundation


struct GameListKickStarter: Codable {
    var games: [GameDataKickStarter]
}

struct GameDataKickStarter: Codable {
    var id: String?
    var name: String?
    var min_players: Int?
    var max_players: Int?
    var min_age: Int?
    var min_playtime: Int?
    var max_playtime: Int?
    var descriptions: String?
    var categories: [CategoryAPI]
    var mechanics: [MechanicAPI]
    var images: ImagesAPI?
    var average_user_rating: Double?
    var ks_id: String?
    var kickstarter_url : String?
    var kickstarter_goal : String?
    var kickstarter_pledge : String?
    var kickstarter_percent : Double
    var kickstarter_deadline_ago : String?
    var kickstarter_num_backers : Int
    var kickstarter_is_over : Bool
    
    init() {
        self.id = "000"
        self.name = "GameName"
        self.average_user_rating = 0.0
        self.min_players = 1
        self.max_players = 9
        self.min_playtime = 15
        self.max_playtime = 45
        self.min_age = 3
        self.descriptions = "Demo Description"
        self.categories = [CategoryAPI()]
        self.mechanics = [MechanicAPI()]
        self.images = ImagesAPI()
        self.ks_id = "000"
        self.kickstarter_url  = "000"
        self.kickstarter_goal  = "000"
        self.kickstarter_pledge = "000"
        self.kickstarter_percent = 0.0
        self.kickstarter_deadline_ago = "000"
        self.kickstarter_num_backers = 1
        self.kickstarter_is_over = false
        
    }
}


func getGameKickStarter() async throws -> GameListKickStarter {
    apiComponents.path = search
    apiComponents.queryItems = [URLQueryItem(name:"kickstarter",value:"true"),clientId]
    
    let (data, response) = try await URLSession.shared.data(from: apiComponents.url ?? defaultUrl)
//    print(data)
//    print("-----------------------")
//    print(response)
    guard let httpResponse = response as?HTTPURLResponse,
          httpResponse.statusCode == 200 else {
              throw URLError(.badServerResponse)
          }
    
    let games = try jsonDecoder.decode(GameListKickStarter.self, from: data)
    var list_games = GameListKickStarter (games: [])
    let lenght = games.games.count
    
    if(games.games.count > 10 ) {
        for i in 1...10 {
            list_games.games.append(games.games[i])
        }
    } else {
        for i in 1...lenght {
            list_games.games.append(games.games[i])
        }
    }
    return list_games

}

func kickstarterExtract(ksGame:GameDataKickStarter) -> Double{
    var kickstarter_percent : Double
    if (ksGame.kickstarter_percent >= 100){
        kickstarter_percent = 100
    } else {
        kickstarter_percent = ksGame.kickstarter_percent
    }
    return kickstarter_percent
}
