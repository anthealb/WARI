////
////  DataHandler.swift
////  WARI
////
////  Created by Anthea Lavinia Bove on 02/04/22.
////
//
//import Foundation
//import SwiftUI
//import CoreData
//
//public class DataHandler: ObservableObject {
//    var context: NSManagedObjectContext
//    var allGames: FetchedResults<GameData>
//    
//    // computed properties
//    var owned: Array<GameData> {
//        return self.allGames.filter { $0.collection == 0 }
//    }
//    var wishlist: Array<GameData> {
//        return self.allGames.filter { $0.collection == 1 }
//    }
//    
//    init(context moc: NSManagedObjectContext) {
//        self.context = moc
//        
//        @FetchRequest (entity: GameData.entity(), sortDescriptors: []) var games: FetchedResults<GameData>
//        self.allGames = games
//    }
//    
//    // get-set functions
//    public func getAllGames() -> [GameData] {
//        return self.allGames.compactMap{ $0 }
//    }
//    
//    public func getCollection(_ type: Int16) -> [GameData] {
//        return self.allGames.filter{ $0.collection == type }
//    }
//    
//    // database handling functions
//    public func save() {
//            do {
//                try self.context.save()
//            } catch {
//                let nsError = error as NSError
//                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
//            }
//        }
//    
//    // check if game is in collection
//    func containsGame(id: String) -> Bool {
//        return false
//    }
//    
//}
