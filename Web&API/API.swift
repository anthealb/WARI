//
//  API.swift
//  prova
//
//  Created by Anthea Lavinia Bove on 11/02/22.
//

import Foundation
import SwiftUI
import WebKit
import Combine

/*API data*/
let boardgameAtlas = "https://api.boardgameatlas.com"
let search = "/api/search"
let clientId = URLQueryItem(name: "client_id", value: "c9Uht5I5yr")
let defaultUrl = URL(string: "https://api.boardgameatlas.com/api/search?client_id=c9Uht5I5yr")!
let defaultImg = "https://i.imgur.com/6NDqr9s.png"
//let defaultImg = "https://st4.depositphotos.com/14953852/24787/v/600/depositphotos_247872612-stock-illustration-no-image-available-icon-vector.jpg"

var apiComponents = URLComponents(string: boardgameAtlas)!
let jsonDecoder = JSONDecoder()

/*Dictionary Categories*/
let cat_dict: [String: String] = ["hBqZ3Ar4RJ" : "Abstract",
                                  "KUBCKBkGxV" : "Adventure",
                                  "PinhJrhnxU" : "Bluffing",
                                  "eX8uuNlQkQ" : "Cards", // components
                                  "HKaYVNIxAJ" : "Children Games",
                                  "G5kfqnPBP6" : "Comic Book/Movies/TV",
//                                  "Sod2YBWMKi" : "Comic Book/Movies/TV", // handle doubles
                                  "mavSOM8vjH" : "Dice", // components
                                  "bCBXJy9qDw" : "Deduction/Murder Mystery",
//                                  "Kk70K0524Z" : "Deduction/Murder Mystery", // handle doubles
                                  "yq6hVlbM2R" : "Exploration",
                                  "dAyk5NtNTV" : "Escape",
                                  "7rV11PKqME" : "Family Game",
                                  "ZTneo8TaIO" : "Fantasy",
                                  "upXZ8vNfNO" : "Fighting",
                                  "cAIkk5aLdQ" : "Horror",
                                  "rrvd68LjOR" : "Kickstarter",
                                  "AujCle9cUq" : "Memory",
                                  "FC6ElKI9tk" : "Miniatures", //components
                                  "X8J7RM6dxX" : "Party Game",
                                  "zyj9ZK3mHB" : "Resource Management",
                                  "3B3QpKvXD3" : "Sci-Fi",
                                  "buDTYyPw4D" : "Territory Building",
                                  "djokexoK0U" : "Video Game Theme",
                                  "jX8asGGR6o" : "War",
]

/*Dictionary Mechanics*/
let mech_dict: [String: String] = ["0ez69aUfuJ" : "Communication Limits", //category
//                                   "GsNGxZFNCK" : "Deduction", //category > handle doubles
                                   "5kvyChnWuO" : "Dexterity", //category
                                   "r6yIFvyXDD" : "Memory", //category
                                   "8PN2HE86wg" : "Tiles", //components
]


/*Data Handling*/
struct GameList: Codable {
    var games: [GameDataAPI]
}

struct GameDataAPI: Codable, Hashable {
    static func == (lhs: GameDataAPI, rhs: GameDataAPI) -> Bool {
        if lhs.id == rhs.id { return true }
        else { return false }
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    var id: String
    var name: String
    var average_user_rating: Double?
    var min_players: Int?
    var max_players: Int?
    var min_playtime: Int?
    var max_playtime: Int?
    var min_age: Int?
    var description: String?
    var categories: [CategoryAPI]
    var mechanics: [MechanicAPI]
    var images: ImagesAPI
    var image_url: String?
    
    init() {
        self.id = "000"
        self.name = "GameName"
        self.average_user_rating = 0.0
        self.min_players = 1
        self.max_players = 9
        self.min_playtime = 15
        self.max_playtime = 45
        self.min_age = 3
        self.description = "Demo Description"
        self.categories = [CategoryAPI()]
        self.mechanics = [MechanicAPI()]
        self.images = ImagesAPI()
    }
    
    init(from game: GameData) {
        self.id = game.getIdGame
        self.name = game.getName
        self.average_user_rating = game.averageUserRating
        self.min_players = game.getMinPlayers
        self.max_players = game.getMaxPlayers
        self.min_playtime = game.getMinPlaytime
        self.max_playtime = game.getMaxPlaytime
        self.min_age = game.getMinAge
        self.description = game.getDescription
        self.categories = [CategoryAPI()]
        self.mechanics = [MechanicAPI()]
        for cat in game.getCategories {
            self.categories.append(CategoryAPI(from: cat))
        }
        for mech in game.getMechanics {
            self.mechanics.append(MechanicAPI(from: mech))
        }
        self.images = ImagesAPI(from: game.getImage)
    }
    
    func getImageURL() -> URL {
        if self.images.medium != nil {
            if self.images.original == "https://s3-us-west-1.amazonaws.com/5cc.images/games/empty+box.jpg" {
                return URL(string: defaultImg)!
            }
            return URL(string: self.images.medium!)!
        } else if self.image_url != nil {
            if self.image_url == "https://s3-us-west-1.amazonaws.com/5cc.images/games/empty+box.jpg" {
                return URL(string: defaultImg)!
            }
            return URL(string: self.image_url!)!
        }
        return URL(string: defaultImg)!
    }
    
    func getDescription() -> String {
        if self.description != nil {
            if self.description != "" {
                return self.description!
            }
        }
        return "Description Not Available"
    }
}

struct CategoryAPI: Codable{
    var id: String
    
    init(){
        self.id = ""
    }
    
    init(from category: Category){
        self.id = category.getIdCategory
    }
}

struct MechanicAPI: Codable{
    var id: String
    
    init(){
        self.id = ""
    }
    
    init(from mechanic: Mechanic){
        self.id = mechanic.getIdMech
    }
}

struct ImagesAPI: Codable {
    var thumb: String?
    var small: String?
    var medium: String?
    var large: String?
    var original: String?
    
    init() {
        self.thumb = defaultImg
        self.small = defaultImg
        self.medium = defaultImg
        self.large = defaultImg
        self.original = defaultImg
    }
    
    init(from imageData: ImageData) {
        self.thumb = imageData.getThumb
        self.small = imageData.getSmall
        self.medium = imageData.getMedium
        self.large = imageData.getLarge
        self.original = imageData.getOriginal
    }
    
//    func defaultImage() -> ImagesAPI {
//        var image = ImagesAPI()
//        image.thumb = defaultImg
//        image.medium = defaultImg
//        image.large = defaultImg
//        image.original = defaultImg
//        image.small = defaultImg
//        
//        return image
//    }
}

struct QueryGame: Codable, Equatable {
    var name: String
    var min_players: Int?
    var lt_max_playtime: Int?
    var categories: String?
    var mechanics: String?
    
    init() {
        self.name = ""
        self.min_players = nil
        self.lt_max_playtime = nil
        self.categories = nil
        self.mechanics = nil
    }
    
    func isEmpty() -> Bool {
        if self.name != "" { return false }
        if self.min_players != nil { return false }
        if self.lt_max_playtime != nil { return false }
        if self.categories != nil { return false }
        if self.mechanics != nil { return false }
        return true
    }
    
    mutating func addCategory(_ cat: String) {
        if self.categories == nil {
            self.categories = cat
        } else {
            self.categories = self.categories! + "," + cat
        }
    }
    
    mutating func addMechanic(_ mech: String) {
        if self.mechanics == nil {
            self.mechanics = mech
        } else {
            self.mechanics = self.mechanics! + "," + mech
        }
    }
}



/*func extract single game data*/
func getGame(_ id: String) async throws -> GameDataAPI? {
    apiComponents.path = search
    apiComponents.queryItems = [URLQueryItem(name: "ids", value: id), clientId]
    
    let (data, response) = try await URLSession.shared.data(from: apiComponents.url ?? defaultUrl)
    
    guard let httpResponse = response as?HTTPURLResponse,
          httpResponse.statusCode == 200 else {
              throw URLError(.badServerResponse)
          }
    
    let games = try jsonDecoder.decode(GameList.self, from: data)
    return games.games[0]
}



/*func extract single random game data*/
func getRandomGame() async throws -> GameDataAPI? {
    apiComponents.path = search
    apiComponents.queryItems = [URLQueryItem(name: "random", value: "true"), clientId]
    
    let (data, response) = try await URLSession.shared.data(from: apiComponents.url ?? defaultUrl)
    
    guard let httpResponse = response as?HTTPURLResponse,
          httpResponse.statusCode == 200 else {
              throw URLError(.badServerResponse)
          }
    
    let games = try jsonDecoder.decode(GameList.self, from: data)
    return games.games[0]
}



/*func extract top 5 kickstarters*/
func getKickstarters() async throws -> [GameDataAPI]? {
    apiComponents.path = search
    apiComponents.queryItems = [URLQueryItem(name: "kickstarter", value: "true"), URLQueryItem(name: "limit", value: "25"), clientId]
    
    let (data, response) = try await URLSession.shared.data(from: apiComponents.url ?? defaultUrl)
    
    guard let httpResponse = response as?HTTPURLResponse,
          httpResponse.statusCode == 200 else {
              throw URLError(.badServerResponse)
          }
    
    let games = try jsonDecoder.decode(GameList.self, from: data)
    return games.games
}



/*func extract top 5 popular*/
func getPopular() async throws -> [GameDataAPI]? {
    apiComponents.path = search
    apiComponents.queryItems = [URLQueryItem(name: "limit", value: "25"), clientId]
    
    let (data, response) = try await URLSession.shared.data(from: apiComponents.url ?? defaultUrl)
    
    guard let httpResponse = response as?HTTPURLResponse,
          httpResponse.statusCode == 200 else {
              throw URLError(.badServerResponse)
          }
    
    let games = try jsonDecoder.decode(GameList.self, from: data)
    return games.games
}



/*func creates strings for future URL API*/
func createQuery(_ querygame: QueryGame) -> [URLQueryItem] {
    
    var queryItems: [URLQueryItem] = [clientId, URLQueryItem(name: "limit", value: "25")]
    
    if querygame.name != "" {
        queryItems.append(URLQueryItem(name: "name", value: querygame.name))
    }
    
    if let min_players = querygame.min_players {
        queryItems.append(URLQueryItem(name: "min_players", value: "\(min_players)"))
    }
    if let lt_max_playtime = querygame.lt_max_playtime {
        queryItems.append(URLQueryItem(name: "lt_max_playtime", value: "\(lt_max_playtime)"))
    }
    if let categories = querygame.categories {
        queryItems.append(URLQueryItem(name: "categories", value: "\(categories)"))
    }
    if let mechanics = querygame.mechanics {
        queryItems.append(URLQueryItem(name: "mechanics", value: "\(mechanics)"))
    }
    
    return queryItems
}



/*func creates URL API and extracts list of games*/
func getGameByQuery(_ queryItems:[URLQueryItem]) async throws -> [GameDataAPI]? {
    apiComponents.path = search
    apiComponents.queryItems = queryItems
    
    let (data, response) = try await URLSession.shared.data(from: apiComponents.url ?? defaultUrl)
    
    guard let httpResponse = response as?HTTPURLResponse,
          httpResponse.statusCode == 200 else {
              throw URLError(.badServerResponse)
          }
    
    let games = try jsonDecoder.decode(GameList.self, from: data)
    
    return games.games
    
}



/*func translate the category code to category name and vice versa*/
func codeCategory(_ category: String) -> String? {
    for (key, value) in cat_dict {
        if (key==category){
            return value
        } else if (value==category){
            return key
        }
    }
    return nil
}


/*func translate the mechanics code to mechanics name and vice versa*/
func codeMechanics(_ mechanic: String) -> String? {
    for (key, value) in mech_dict {
        if (key==mechanic){
            return value
        }else if (value==mechanic){
            return key
        }
    }
    return nil
}

func queryTagHelper(_ tag: String, query: QueryGame?) -> QueryGame {
    var newQuery: QueryGame
    
    if query == nil {
        newQuery = QueryGame()
    } else {
        newQuery = query!
    }
    if let code = codeCategory(tag) {
        newQuery.addCategory(code)
        return newQuery
    }
    if let code = codeMechanics(tag) {
        newQuery.addMechanic(code)
        return newQuery
    }
    return QueryGame()
}

/* func that checks if the image game is available or not */
//func checkImage(_ image: ImagesAPI) -> ImagesAPI{
//    var image = image
//    if image.original == "https://s3-us-west-1.amazonaws.com/5cc.images/games/empty+box.jpg" {
//        image = image.defaultImage()
//    }
//    return image
//}
