//
//  WARIApp.swift
//  WARI
//
//  Created by Anthea Lavinia Bove on 06/02/22.
//

import SwiftUI

@main
struct WARIApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
//                .environment(\.locale, .init(identifier: "it"))
        }
    }
}
